﻿using UnityEngine;

/// <summary>
/// Class representing mob injury decision for pluggable AI.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Decisions/Injury")]
public class InjuryDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool isInjured = CheckInjury(controller);
        return isInjured;
    }

    private bool CheckInjury(StateController controller)
    {
        return controller.MobHealthManager.GetHasBeenInjured();
    }
}
