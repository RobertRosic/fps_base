﻿using UnityEngine;

/// <summary>
/// Abstract class representing an AI action.
/// </summary>
public abstract class AiAction : ScriptableObject
{
    public abstract void Act(StateController controller);
}
