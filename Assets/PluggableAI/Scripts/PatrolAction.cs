﻿using UnityEngine;

/// <summary>
/// Class represeting patrol AI action.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol")]
public class PatrolAction : AiAction
{
    public override void Act(StateController controller)
    {
        Patrol(controller);
    }

    private void Patrol(StateController controller)
    {
        controller.MobMoveController.Patrol();
    }
}
