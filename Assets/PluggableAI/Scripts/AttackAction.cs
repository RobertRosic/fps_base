﻿using UnityEngine;

/// <summary>
/// Class representing the attack AI action.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/Actions/Attack")]
public class AttackAction : AiAction
{
    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    private void Attack(StateController controller)
    {
        controller.MobMoveController.RotateTowardsTarget(controller.ChaseTarget);
        controller.MobWeaponController.StartAttack(controller.ChaseTarget);
    }
}
