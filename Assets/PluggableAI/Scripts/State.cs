﻿using UnityEngine;

/// <summary>
/// Class representing a state in the PluggableAI FSM.
/// </summary>
[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject
{
    public AiAction[] actions;
    public Transition[] transitions;
    public Color sceneGizmoColor = Color.gray;

    /// <summary>
    /// Do all actions and check transitions.
    /// </summary>
    /// <param name="controller"></param>
    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    /// <summary>
    /// Call every action's Act() method.
    /// </summary>
    /// <param name="controller">StateController reference</param>
    private void DoActions(StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    /// <summary>
    /// Check every transition by calling its decision's Decide() method.
    /// </summary>
    /// <param name="controller">StateController reference</param>
    private void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }
}
