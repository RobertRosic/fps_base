﻿/// <summary>
/// Transition class with true, false states and a decision.
/// </summary>
[System.Serializable]
public class Transition
{
    public Decision decision;
    public State trueState;
    public State falseState;
}
