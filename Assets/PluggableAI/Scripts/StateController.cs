﻿using System;
using UnityEngine;

/// <summary>
/// State controller class for PluggableAI.
/// </summary>
public class StateController : MonoBehaviour
{
    [SerializeField]
    private State currentState;
    [SerializeField]
    private State remainState;
    [SerializeField]
    private Transform chaseTarget;
    [SerializeField]
    private bool aiActive = true;


    private float stateTimeElapsed;    
    private FieldOfView fieldOfView;
    private MobMoveController mobMoveController;
    private MobWeaponController mobWeaponController;
    private MobSoundController mobSoundController;
    private MobHealthManager mobHealthManager;

    public FieldOfView FieldOfView
    {
        get
        {
            return fieldOfView;
        }
    }

    public MobWeaponController MobWeaponController
    {
        get
        {
            return mobWeaponController;
        }
    }

    public MobMoveController MobMoveController
    {
        get
        {
            return mobMoveController;
        }
    }

    public MobHealthManager MobHealthManager
    {
        get
        {
            return mobHealthManager;
        }
    }

    public Transform ChaseTarget
    {
        get
        {
            return chaseTarget;
        }
    }

    /// <summary>
    /// Set component references.
    /// </summary>
    private void Awake()
    {
        fieldOfView = GetComponent<FieldOfView>();
        mobMoveController = GetComponent<MobMoveController>();
        mobWeaponController = GetComponent<MobWeaponController>();
        mobSoundController = GetComponent<MobSoundController>();
        mobHealthManager = GetComponent<MobHealthManager>();
    }

    /// <summary>
    /// Call UpdateState() on current state.
    /// </summary>
    private void FixedUpdate()
    {
        if (aiActive)
        {
            currentState.UpdateState(this);
        }        
    }

    /// <summary>
    /// Show gizmo with the appropriate color of the current state.
    /// </summary>
    private void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(gameObject.transform.position, 2f);
        }
    }

    /// <summary>
    /// Transition to state and play sound clip associated with next state.
    /// </summary>
    /// <param name="nextState"></param>
    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;

            mobSoundController.PlaySound((int)Enum.Parse(typeof(DroneSound), nextState.name));

            OnExitState();
        }
    }

    /// <summary>
    /// Check if time spent in the current state elapsed a certain duration/
    /// </summary>
    /// <param name="duration">Duration</param>
    /// <returns>True if time has exceeded the duration</returns>
    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return stateTimeElapsed >= duration;
    }

    /// <summary>
    /// Set time spent in state back to 0.
    /// </summary>
    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }
}