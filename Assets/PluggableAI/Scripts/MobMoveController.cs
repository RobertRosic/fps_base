﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Move controller class for mobs.
/// </summary>
public class MobMoveController : MonoBehaviour
{
    [SerializeField]
    private float turnSpeed = 1f;
    [SerializeField]
    private List<Transform> waypointList;
    [SerializeField]
    private int nextWaypoint;

    private NavMeshAgent navMeshAgent;
    private Vector3 destination;

    public List<Transform> WaypointList
    {
        get
        {
            return waypointList;
        }
    }

    public int NextWaypoint
    {
        get
        {
            return nextWaypoint;
        }
    }

    /// <summary>
    /// Set NavMeshAgent reference, set destination to first waypoint, set NavMeshAgent destination.
    /// </summary>
    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        destination = waypointList[nextWaypoint].position;
        navMeshAgent.SetDestination(destination);
    }

    /// <summary>
    /// Reset navmesh settings if necessary, switch to next waypoint and set it as destination.
    /// </summary>
    public void Patrol()
    {
        if (navMeshAgent.stoppingDistance != 0)
        {
            navMeshAgent.stoppingDistance = 0f;
        }

        if (navMeshAgent.isStopped)
        {
            ReturnToPatrol();
        }

        if (Vector3.Distance(transform.position, destination) < 2.0f)
        {
            nextWaypoint = (nextWaypoint + 1) % waypointList.Count;
            destination = waypointList[nextWaypoint].position;
            navMeshAgent.SetDestination(destination);
        }
    }

    /// <summary>
    /// Resume patrolling by moving to the nearest waypoint.
    /// </summary>
    private void ReturnToPatrol()
    {
        navMeshAgent.isStopped = false;

        destination = waypointList.OrderBy(wp => Vector3.Distance(transform.position,
            wp.position)).First().position;

        navMeshAgent.SetDestination(destination);
    }

    /// <summary>
    /// Move mob to destination with preset stopping distance.
    /// </summary>
    /// <param name="destination">Destination coordinates</param>
    /// <param name="stoppingDistance">Stoppig distance from destination</param>
    public void MoveToDestination(Vector3 destination, float stoppingDistance = 0f)
    {
        navMeshAgent.stoppingDistance = stoppingDistance;
        navMeshAgent.SetDestination(destination);
    }

    /// <summary>
    /// Stop movement.
    /// </summary>
    public void Stop()
    {
        navMeshAgent.isStopped = true;
    }

    /// <summary>
    /// Lerp the mob rotation towards target transform.
    /// </summary>
    /// <param name="target">Target transform</param>
    public void RotateTowardsTarget(Transform target)
    {
        Vector3 direction = target.position - transform.position;
        direction.y = 0f;
        transform.rotation = Quaternion.Lerp(transform.rotation,
            Quaternion.LookRotation(direction), Time.deltaTime * turnSpeed);
    }
}