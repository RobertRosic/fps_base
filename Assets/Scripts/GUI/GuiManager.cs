﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// GUI manager class.
/// </summary>
public class GuiManager : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup gameOverPanel;
    [SerializeField]
    private Text playerHealthText;
    [SerializeField]
    private Image damageIndicator;
    [SerializeField]
    private Text playerAmmoText;
    [SerializeField]
    private float damageIndicatorFlashLength;
    [SerializeField]
    private AudioListener guiAudioListener;
    [SerializeField]
    private CanvasGroup helpScreen;

    /// <summary>
    /// Add event listeners.
    /// </summary>
    private void OnEnable()
    {
        Messenger.AddListener(GameEvent.GameOver, OnGameOver);
        Messenger<int, bool>.AddListener(GameEvent.PlayerHealthChange, OnPlayerHealthChange);
        Messenger<int>.AddListener(GameEvent.PlayerAmmoChange, OnPlayerAmmoChange);
    }

    /// <summary>
    /// Remove event listeners.
    /// </summary>
    private void OnDisable()
    {
        Messenger.RemoveListener(GameEvent.GameOver, OnGameOver);
        Messenger<int, bool>.RemoveListener(GameEvent.PlayerHealthChange, OnPlayerHealthChange);
        Messenger<int>.RemoveListener(GameEvent.PlayerAmmoChange, OnPlayerAmmoChange);
    }

    /// <summary>
    /// Initialize damage indicator's alpha (required for CrossFadeAlpha()).
    /// </summary>
    private void Awake()
    {
        damageIndicator.canvasRenderer.SetAlpha(0f);
    }

    /// <summary>
    /// Toggle help screen visibility.
    /// </summary>
    public void ToggleHelpScreen()
    {
        helpScreen.alpha = (helpScreen.alpha > 0f) ? 0f : 1f;
    }
    
    /// <summary>
    /// Event handler for level restart button.
    /// </summary>
    public void OnRestartButtonClick()
    {
        guiAudioListener.enabled = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Game over event handler.
    /// </summary>
    private void OnGameOver()
    {
        gameOverPanel.alpha = 1f;
        gameOverPanel.interactable = true;
        guiAudioListener.enabled = true;
    }

    /// <summary>
    /// Player health change event handler.
    /// </summary>
    /// <param name="health">Current health value</param>
    /// <param name="isDamage">Boolean indicating if player was damaged</param>
    private void OnPlayerHealthChange(int health, bool isDamage)
    {
        if (isDamage)
        {
            StartCoroutine(FlashDamageIndicator());
        }

        playerHealthText.text = health.ToString();
    }

    /// <summary>
    /// Flash damage indicator panel for a short time.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator FlashDamageIndicator()
    {
        damageIndicator.CrossFadeAlpha(120f, damageIndicatorFlashLength, false);
        yield return new WaitForSeconds(damageIndicatorFlashLength);
        damageIndicator.CrossFadeAlpha(0f, damageIndicatorFlashLength, false);
    }

    /// <summary>
    /// Event handler for player ammo change.
    /// </summary>
    /// <param name="ammo">Current ammo value</param>
    private void OnPlayerAmmoChange(int ammo)
    {
        playerAmmoText.text = ammo.ToString();
    }
}
