﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    [SerializeField]
    private bool isTestModeEnabled = false;

    public static ObjectPoolManager Current;
    private Dictionary<GameObject, ObjectPool> ObjectPools;

    /// <summary>
    /// Set public static reference. Initialize dictionary of object pools. If the containing
    /// GameObject has pools attached, add them to ObjectPools.
    /// </summary>
    private void Awake()
    {
        ObjectPools = new Dictionary<GameObject, ObjectPool>();
        Current = this;

        foreach (var item in GetComponents<ObjectPool>())
        {
            ObjectPools.Add(item.PooledType, item);
        }
    }

    /// <summary>
    /// If an object pool exists for the required type, try to get an instance from it. If no such
    /// object pool exists, create one and add its first element.
    /// </summary>
    /// <param name="obj">Type of pooled GameObject to get</param>
    /// <returns>A pooled GameObject instance (or null if pool is full and not permitted to grow)
    /// </returns>
    public GameObject GetPooledObject(GameObject obj)
    {
        if (ObjectPools.ContainsKey(obj))
        {
            return ObjectPools[obj].GetPooledObject(isTestModeEnabled);
        }
        else
        {
            var objectPool = gameObject.AddComponent<ObjectPool>();
            objectPool.PooledType = obj;
            ObjectPools.Add(obj, objectPool);
            return objectPool.GetPooledObject(isTestModeEnabled);
        }      
    }
}
