﻿using UnityEngine;

/// <summary>
/// Ammo pickup class.
/// </summary>
public class AmmoPickup : MonoBehaviour
{
    [SerializeField]
    private int amount;

    private void OnTriggerEnter(Collider other)
    {
        GameObject otherObject = other.gameObject;

        if (other.GetComponent<Tags>().Player)
        {
            AmmoManager ammoManager = otherObject.GetComponent<AmmoManager>();
            otherObject.GetComponent<AmmoManager>().ChangeAmmo(amount);
            Destroy(gameObject);
        }
    }
}
