﻿using UnityEngine;

/// <summary>
/// Health pickup class.
/// </summary>
public class HealthPickup : MonoBehaviour
{
    [SerializeField]
    private int amount;

    private void OnTriggerEnter(Collider other)
    {
        GameObject otherObject = other.gameObject;

        if (other.GetComponent<Tags>().Player)
        {
            PlayerHealthManager healthManager = otherObject.GetComponent<PlayerHealthManager>();

            if (healthManager.Health < healthManager.MaxHealth)
            {
                otherObject.GetComponent<PlayerHealthManager>().Heal(amount);
                Destroy(gameObject);
            }
        }
    }
}
