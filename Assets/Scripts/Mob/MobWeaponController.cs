﻿using UnityEngine;

/// <summary>
/// Mob weapon controller class.
/// </summary>
public class MobWeaponController : MonoBehaviour
{
    private MobWeapon[] weapons;

    /// <summary>
    /// Get all weapons of mob.
    /// </summary>
    private void Start()
    {
        weapons = GetComponentsInChildren<MobWeapon>();
    }

    /// <summary>
    /// Attack with all weapons.
    /// </summary>
    public void StartAttack(Transform target)
    {
        foreach (MobWeapon weapon in weapons)
        {
            weapon.Attack(target);
        }
    }

    /// <summary>
    /// Stop all weapons.
    /// </summary>
    public void StopAttack()
    {
        foreach (MobWeapon weapon in weapons)
        {
            weapon.StopAttack();
        }
    }
}
