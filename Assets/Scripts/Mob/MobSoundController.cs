﻿using UnityEngine;

/// <summary>
/// Sound controller class for mobs.
/// </summary>
public class MobSoundController : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] audioClips;

    private AudioSource audioSource;

    /// <summary>
    /// Get AudioSource reference.
    /// </summary>
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Play a clip from the previously set array of mob sounds.
    /// </summary>
    /// <param name="soundIndex">Index of audio clip</param>
    public void PlaySound(int soundIndex)
    {
        audioSource.PlayOneShot(audioClips[soundIndex]);
    }
}

public enum DroneSound
{
    AlertScanner,
    ChaseScanner,
    PatrolScanner
}
