﻿using System.Collections;
using UnityEngine;

public class HitFlareLogic : MonoBehaviour
{    
    private void OnEnable()
    {
        StartCoroutine(DisableAfterDuration(1f));
    }

    private IEnumerator DisableAfterDuration(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}
