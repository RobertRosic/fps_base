﻿using UnityEngine;

/// <summary>
/// Class for automatic weapons. Shoot with bullet spread.
/// </summary>
class Automatic : Weapon
{    
    protected override void CalculateShot()
    {
        ammoManager.ChangeAmmo(-1);

        if (Physics.Raycast(rayOrigin, CalculateBulletSpread() * Vector3.forward, out hit,
            gunStats.range, gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}

