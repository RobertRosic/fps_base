﻿using UnityEngine;

/// <summary>
/// Data object for gun stats.
/// </summary>
[CreateAssetMenu(menuName = "GunStats")]
public class GunStats : ScriptableObject
{
    public bool isAutomatic;
    public int damage;
    public float fireRate;
    public float range;
    public float hitForce;
    public float maxBulletSpreadAngle;
    public float timeUntilMaxSpreadAngle;
    public LayerMask layerMask;
}
