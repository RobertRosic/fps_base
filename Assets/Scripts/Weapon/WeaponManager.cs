﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Player weapon manager class.
/// </summary>
public class WeaponManager : MonoBehaviour
{
    [SerializeField]
    protected GameObject hitMarker;
    [SerializeField]
    private Weapon[] weapons;

    private float firingSince;
    private int currentWeaponIndex = 0;

    public float WeaponRange
    {
        get
        {
            return weapons[currentWeaponIndex].Range;
        }
    }

    private void Start()
    {
        weapons[currentWeaponIndex].gameObject.SetActive(true);
    }

    private void LateUpdate()
    {
        OnWeaponFire();
        OnWeaponChange();
    }

    /// <summary>
    /// Change weapon if the corresponding input is received.
    /// </summary>
    private void OnWeaponChange()
    {
        float scrollWheelAxisValue = JoystickAxisDataManager.AxisGetButtonDown("D-Pad Y Axis");

        if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") > .0f || scrollWheelAxisValue > .0f)
        {
            weapons[currentWeaponIndex].gameObject.SetActive(false);
            currentWeaponIndex = (currentWeaponIndex + 1) % weapons.Length;
            weapons[currentWeaponIndex].gameObject.SetActive(true);
        }
        else if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") < .0f || scrollWheelAxisValue < .0f)
        {
            weapons[currentWeaponIndex].gameObject.SetActive(false);

            if (currentWeaponIndex == 0)
            {
                currentWeaponIndex = weapons.Length - 1;
            }
            else
            {
                currentWeaponIndex = (currentWeaponIndex - 1) % weapons.Length;
            }

            weapons[currentWeaponIndex].gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Weapon fire logic. If the weapon is automatic, fire while fire button is held down.
    /// </summary>
    private void OnWeaponFire()
    {
        if (weapons[currentWeaponIndex].IsAutomatic)
        {
#if MOBILE_INPUT
            if (CrossPlatformInputManager.GetButton("Mobile Fire1"))
#else
            if (Input.GetButton("Fire1") || Input.GetAxis("Joystick Fire1") != .0f)
#endif
            {
                firingSince += Time.deltaTime;
                weapons[currentWeaponIndex].Shoot(firingSince);
            }
            else
            {
                firingSince = .0f;
            }
        }
        else
        {
#if MOBILE_INPUT
            if (CrossPlatformInputManager.GetButtonDown("Mobile Fire1"))
#else

            float joyFire1AxisValue = JoystickAxisDataManager.AxisGetButtonDown("Joystick Fire1");

            if (Input.GetButtonDown("Fire1") || joyFire1AxisValue != .0f)
#endif
            {
                weapons[currentWeaponIndex].Shoot(null);
            }
        }
    }
}