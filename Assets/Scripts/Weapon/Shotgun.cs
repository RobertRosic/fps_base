﻿using UnityEngine;

/// <summary>
/// Class for shotgun weapons. Shoot multiple bullets in quick succession with spread.
/// </summary>
class Shotgun : Weapon
{
    [SerializeField]
    private int shotsPerBullet = 10;

    protected override void CalculateShot()
    {
        firingSince = null;

        for (int i = 0; i < shotsPerBullet; i++)
        {
            if (ammoManager.Ammo > 0)
            {
                ammoManager.ChangeAmmo(-1);

                if (Physics.Raycast(rayOrigin, CalculateBulletSpread() * Vector3.forward, out hit,
                    gunStats.range, gunStats.layerMask))
                {
                    OnRayCastHit();
                }
            }
        }
    }
}

