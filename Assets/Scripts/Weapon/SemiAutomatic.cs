﻿using UnityEngine;

/// <summary>
/// Class for semi-automatic weapons. Shoot without bullet spread.
/// </summary>
class SemiAutomatic : Weapon
{
    protected override void CalculateShot()
    {
        ammoManager.ChangeAmmo(-1);

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, gunStats.range,
            gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}

