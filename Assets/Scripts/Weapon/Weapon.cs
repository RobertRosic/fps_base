﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Abstract weapon base class.
/// </summary>
public abstract class Weapon : MonoBehaviour
{
    [SerializeField]
    protected GunStats gunStats;
    [SerializeField]
    protected GameObject hitMarkerPrefab;
    [SerializeField]
    protected AudioClip emptyGun;

    protected AmmoManager ammoManager;
    protected AudioSource weaponSound;
    protected Camera fpsCam;
    protected Vector3 rayOrigin;
    protected RaycastHit hit;
    protected bool readyToFire = true;
    protected float? firingSince;

    public bool IsAutomatic
    {
        get
        {
            return gunStats.isAutomatic;
        }
    }

    public float Range
    {
        get
        {
            return gunStats.range;
        }
    }

    /// <summary>
    /// Set camera, AudioSource and ammo manager references.
    /// </summary>
    private void Start()
    {
        Debug.Assert(gameObject.layer == LayerMask.NameToLayer("Gun"), "Gun should be on gun layer.");
        fpsCam = GetComponentInParent<Camera>();
        weaponSound = GetComponentInParent<AudioSource>();
        ammoManager = GetComponentInParent<AmmoManager>();
    }

    /// <summary>
    /// If gun is ready to fire, invoke shot ray calculation.
    /// </summary>
    /// <param name="firingSince">If weapon is automatic, it is the time since the weapon is firing.
    /// Otherwise it's null</param>
    public void Shoot(float? firingSince)
    {
        if (readyToFire)
        {
            if (ammoManager.Ammo > 0)
            {
                this.firingSince = firingSince;
                readyToFire = false;
                Invoke("SetReadyToFire", gunStats.fireRate);

                rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, .0f));
                CalculateShot();
                weaponSound.Play();
            }
            else
            {
                this.firingSince = firingSince;
                readyToFire = false;
                Invoke("SetReadyToFire", gunStats.fireRate);
                weaponSound.PlayOneShot(emptyGun);
            }
        }
    }

    protected abstract void CalculateShot();

    /// <summary>
    /// Create bullet spread by rotating shot direction randomly. In the case of automatic weapons,
    /// increase spread over time.
    /// </summary>
    /// <returns>The rotation to be used during the shot</returns>
    protected Quaternion CalculateBulletSpread()
    {
        Quaternion fireRotation = Quaternion.LookRotation(transform.forward);
        Quaternion randomRotation = Random.rotation;

        float currentSpread;

        if (firingSince == null)
        {
            currentSpread = gunStats.maxBulletSpreadAngle;
        }
        else
        {
            currentSpread = Mathf.Lerp(.0f, gunStats.maxBulletSpreadAngle, (float)firingSince / gunStats.timeUntilMaxSpreadAngle);
        }

        fireRotation = Quaternion.RotateTowards(fireRotation, randomRotation, currentSpread);
        return fireRotation;
    }

    protected void SetReadyToFire()
    {
        readyToFire = true;
    }

    /// <summary>
    /// If target has HealthManager, apply damage. If it has rigidbody, apply force. Instantiate
    /// hit marker object.
    /// </summary>
    protected void OnRayCastHit()
    {
        MobHealthManager targetHealthManager = hit.transform.gameObject.GetComponent<MobHealthManager>();

        if (targetHealthManager != null)
        {
            targetHealthManager.Damage(gunStats.damage);
        }

        if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForce(-hit.normal * gunStats.hitForce);
        }

        GameObject hitMarker = ObjectPoolManager.Current.GetPooledObject(hitMarkerPrefab);
        hitMarker.transform.position = hit.point;
        hitMarker.transform.rotation = Quaternion.LookRotation(hit.normal);
        hitMarker.SetActive(true);        
    }
}
