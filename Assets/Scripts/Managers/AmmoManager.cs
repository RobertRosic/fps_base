﻿using UnityEngine;

/// <summary>
/// Player ammon manager class.
/// </summary>
public class AmmoManager : MonoBehaviour
{
    [SerializeField]
    private int ammo;

    /// <summary>
    /// Broadcast current ammo value on start.
    /// </summary>
    private void Start()
    {
        Messenger<int>.Broadcast(GameEvent.PlayerAmmoChange, ammo);
    }

    public int Ammo
    {
        get
        {
            return ammo;
        }
    }

    /// <summary>
    /// Change ammo value and broadcast new quantity.
    /// </summary>
    /// <param name="amount">Amount of change</param>
    public void ChangeAmmo(int amount)
    {
        ammo += amount;
        Messenger<int>.Broadcast(GameEvent.PlayerAmmoChange, ammo);
    }
}
