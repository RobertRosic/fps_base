﻿using UnityEngine;

/// <summary>
/// Player health manager class.
/// </summary>
public class PlayerHealthManager : HealthManager
{
    [SerializeField]
    private AudioClip injurySound;

    private int maxHealth = 100;
    private AudioSource audioSource;

    public int MaxHealth
    {
        get
        {
            return maxHealth;
        }
    }

    /// <summary>
    /// Broadcast health value on start, set AudioSource reference.
    /// </summary>
    private void Start()
    {
        Messenger<int, bool>.Broadcast(GameEvent.PlayerHealthChange, health, false);
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Set new health value when damaged, broadcast the new value, play injury sound. If health below
    /// 0 trigger game over event.
    /// </summary>
    /// <param name="amount">Amount of danage</param>
    public override void Damage(int amount)
    {
        health -= amount;
        Messenger<int, bool>.Broadcast(GameEvent.PlayerHealthChange, health, true);
        audioSource.PlayOneShot(injurySound);

        if (health <= 0)
        {
            gameObject.SetActive(false);
            Messenger.Broadcast(GameEvent.GameOver);
        }
    }

    /// <summary>
    /// Set new health value when healed, limit health to maxhealth, boradcast new value.
    /// </summary>
    /// <param name="heal">Amount of healing</param>
    public override void Heal(int amount)
    {
        int increasedHealth = health + amount;
        health = (increasedHealth <= maxHealth) ? increasedHealth : maxHealth;
        Messenger<int, bool>.Broadcast(GameEvent.PlayerHealthChange, health, false);
    }
}
