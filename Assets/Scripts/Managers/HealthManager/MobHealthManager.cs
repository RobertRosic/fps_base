﻿using UnityEngine;

/// <summary>
/// Mob health manager class.
/// </summary>
public class MobHealthManager : HealthManager
{
    [SerializeField]
    private GameObject deathExplosion;

    private int previousHealth;
    private int currentHealth;

    private void Start()
    {
        previousHealth = health;
        currentHealth = health;
    }

    /// <summary>
    /// Set new health value when damaged, if health below 0 instantiate explosion, deactivate then
    /// destroy mob.
    /// </summary>
    /// <param name="damage">Amount of damage</param>
    public override void Damage(int damage)
    {
        health -= damage;
        currentHealth = health;

        if (health <= 0)
        {
            Instantiate(deathExplosion, gameObject.transform.position, Quaternion.identity);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Set new health value when healed.
    /// </summary>
    /// <param name="heal">Amount of healing</param>
    public override void Heal(int heal)
    {
        health += heal;
        currentHealth = health;
    }

    /// <summary>
    /// Check if the mob has been injured since the last check.
    /// </summary>
    /// <returns>True if current health is less than previous health</returns>
    public bool GetHasBeenInjured()
    {
        if (currentHealth < previousHealth)
        {
            previousHealth = currentHealth;
            return true;
        }
        else
        {
            return false;
        }
    }
}
