﻿using UnityEngine;

/// <summary>
/// Sliding door logic script.
/// </summary>
public class DoorLogic : MonoBehaviour
{
    [SerializeField]
    private float doorSpeed;
    [SerializeField]
    private GameObject movingPart;
    [SerializeField]
    private AudioClip openSound;
    [SerializeField]
    private AudioClip closeSound;

    private Vector3 openPosition;
    private Vector3 closedPosition;
    private bool isOpening = false;
    private AudioSource audioSource;

    /// <summary>
    /// Set closed and open position and AudioSource reference.
    /// </summary>
    private void Start()
    {
        closedPosition = movingPart.transform.position;
        openPosition = movingPart.transform.position + (movingPart.transform.forward * -4f);
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Call MoveDoor() with the required position.
    /// </summary>
    private void Update()
    {
        if (isOpening)
        {
            MoveDoor(openPosition);
        }
        else
        {
            MoveDoor(closedPosition);
        }
    }

    /// <summary>
    /// Lerp door to target position.
    /// </summary>
    /// <param name="targetPosition">Target position</param>
    private void MoveDoor(Vector3 targetPosition)
    {
        movingPart.transform.position = Vector3.Lerp(movingPart.transform.position, targetPosition,
                        Time.deltaTime * doorSpeed);
    }

    /// <summary>
    /// Set to opening if trigger collides with player.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Tags>().Player)
        {
            isOpening = true;
            audioSource.PlayOneShot(openSound);
        }
    }

    /// <summary>
    /// Set to closing if player leaves trigger.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Tags>().Player)
        {
            isOpening = false;
            audioSource.PlayOneShot(closeSound);
        }
    }
}
