﻿public static class GameEvent
{
    public const string GameOver = "GameOver";
    public const string ClimbUpStarted = "ClimbUpStarted";
    public const string ClimbUpFinished = "ClimbUpFinished";
    public const string ClimbUpTriggered = "ClimbUpTriggered";
    public const string LadderToggleMovement = "LadderToggleMovement";
    public const string PlayerHealthChange = "PlayerHealthChange";
    public const string PlayerAmmoChange = "PlayerAmmoChange";
}