﻿using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// GetButtonDown substitute for joystick.
/// </summary>
public static class JoystickAxisDataManager
{
    private static Dictionary<string, float> axisValues = new Dictionary<string, float>();

    /// <summary>
    /// Imitates GetButtonDown() method for joystick axes. Returns a value if it has changed since
    /// last check. Otherwise the method returns 0.
    /// </summary>
    /// <param name="axisName">Name of joystick axis</param>
    /// <returns>A non-zero axis value if it has changed since last check or it's the first time
    /// this value is being checked. 0 otherwise</returns>
    public static float AxisGetButtonDown(string axisName)
    {
        float currentAxisValue = CrossPlatformInputManager.GetAxis(axisName);

        if (axisValues.ContainsKey(axisName))
        {
            if (axisValues[axisName] != currentAxisValue)
            {
                axisValues[axisName] = currentAxisValue;
                return currentAxisValue;
            }
            else
            {
                return 0f;
            }
        }
        else
        {
            axisValues[axisName] = currentAxisValue;
            return currentAxisValue;
        }
    }
}
