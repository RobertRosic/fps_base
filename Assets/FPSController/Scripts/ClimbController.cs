﻿using UnityEngine;

/// <summary>
/// Controller class for climbing logic. Checks climbing conditions.
/// </summary>
public class ClimbController : MonoBehaviour
{
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private CinematicCamManager cinematicCamManager;
    [SerializeField]
    private float overlapBoxYTranslation;
    [SerializeField]
    private float overlapBoxForwardTranslation;

    private CharacterController characterController;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    /// <summary>
    /// Draw a box to visualize climbing overlap.
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        if (transform != null)
        {
            Gizmos.color = Color.red;
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawWireCube(new Vector3(0f, overlapBoxYTranslation,
                overlapBoxForwardTranslation), new Vector3(2f, 2f, 2f));
        }
    }

    /// <summary>
    /// If the character controller hits a collider, check if it's a climbable, the angle is right
    /// and climbing is not obstructed. Broadcast ClimbUpTriggered on success.
    /// </summary>
    /// <param name="hit">Hit object</param>
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (
            IsCollidingWithClimbable(hit) &&
            Vector3.Angle(-hit.normal, transform.forward) <= 30f &&
            !IsClimbingObstructed()
           )
        {
            Messenger.Broadcast(GameEvent.ClimbUpTriggered);
        }
    }

    /// <summary>
    /// If character controller not grounded, colliding via sides and hit object has
    /// the climbable tag, then it is a climbable.
    /// </summary>
    /// <param name="hit">Hit object</param>
    /// <returns>True if it's a climbable</returns>
    private bool IsCollidingWithClimbable(ControllerColliderHit hit)
    {
        return !characterController.isGrounded &&
            characterController.collisionFlags == CollisionFlags.CollidedSides &&
            hit.gameObject.GetComponent<Tags>().Climbable;
    }

    /// <summary>
    /// Checks if the climbing box overlaps with any obstacles.
    /// </summary>
    /// <returns>True if there is an overlapping obstacle within climbing box</returns>
    private bool IsClimbingObstructed()
    {
        Collider[] results = new Collider[3];

        return Physics.OverlapBoxNonAlloc(new Vector3(transform.position.x,
            transform.position.y + overlapBoxYTranslation, transform.position.z) +
            transform.forward * overlapBoxForwardTranslation,
            Vector3.one, results, transform.rotation, layerMask) != 0;
    }
}
