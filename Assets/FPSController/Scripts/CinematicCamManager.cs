﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Manager class for the cinematic camera. Used for climbing animations.
/// </summary>
public class CinematicCamManager : MonoBehaviour
{
    [SerializeField]
    private CharacterController characterController;
    [SerializeField]
    private Camera gunCamera;

    private Camera cinematicCamera;
    private Animator cinematicCamAnimator;

    /// <summary>
    /// Add event handler for climbing event.
    /// </summary>
    private void OnEnable()
    {
        Messenger.AddListener(GameEvent.ClimbUpTriggered, OnTriggerClimbUp);
    }

    /// <summary>
    /// Remove event handler for climbing event.
    /// </summary>
    private void OnDisable()
    {
        Messenger.RemoveListener(GameEvent.ClimbUpTriggered, OnTriggerClimbUp);
    }

    /// <summary>
    /// Get camera and animator references.
    /// </summary>
    private void Start()
    {
        cinematicCamera = GetComponent<Camera>();
        cinematicCamAnimator = GetComponent<Animator>();
    }

    /// <summary>
    /// Event handler for climbing. Enable cinematic cam, trigger climb up animation. Disable character
    /// controller and gun camera, broadcast ClimbUpStarted event.
    /// </summary>
    private void OnTriggerClimbUp()
    {
        cinematicCamera.enabled = true;
        cinematicCamera.depth = 3f;
        cinematicCamAnimator.SetTrigger("ClimbUp");

        characterController.enabled = false;
        Messenger.Broadcast(GameEvent.ClimbUpStarted);
        gunCamera.enabled = false;
    }

    /// <summary>
    /// Method called at the end of climb up animation. Move character controller to the new position,
    /// start coroutine WaitBeforeCharacterControllerEnable().
    /// </summary>
    public void MoveCharacterToCinematicPosition()
    {
        characterController.transform.position =
            new Vector3(transform.position.x,
            transform.position.y - characterController.height,
            transform.position.z);

        StartCoroutine(WaitBeforeCharacterControllerEnable());
    }

    /// <summary>
    /// Disable cinematic cam, enable gun cam, broadcast ClimbUpFinished event, wait a bit, then enable
    /// character controller.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator WaitBeforeCharacterControllerEnable()
    {
        cinematicCamera.enabled = false;
        cinematicCamera.depth = 0f;
        gunCamera.enabled = true;

        Messenger.Broadcast(GameEvent.ClimbUpFinished);

        yield return new WaitForSeconds(0.2f);

        characterController.enabled = true;
    }    
}
