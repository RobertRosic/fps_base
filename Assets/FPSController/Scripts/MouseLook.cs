using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.FirstPerson
{
    /// <summary>
    /// Mouselook logic manager.
    /// </summary>
    [Serializable]
    public class MouseLook : IDisposable
    {
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        public bool lockCursor = true;

        private string xAxis;
        private string yAxis;

        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        private bool m_cursorIsLocked = true;

        /// <summary>
        /// Initialize rotations, add listener for game over event.
        /// </summary>
        /// <param name="character"></param>
        /// <param name="camera"></param>
        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;

            Messenger.AddListener(GameEvent.GameOver, OnGameOver);
        }

        /// <summary>
        /// Rotates the camera in the direction provided by mouse or joystick movement.
        /// </summary>
        /// <param name="character">Reference to the character's transform</param>
        /// <param name="camera">Reference to the camera's transform</param>
        /// <param name="shouldResetCamRotation">If true, the camera rotation is reset to a default value</param>
        public void LookRotation(Transform character, Transform camera, bool shouldResetCamRotation)
        {
            // Reset cam rotation
            if (shouldResetCamRotation)
            {
                m_CameraTargetRot = Quaternion.LookRotation(new Vector3(0f, 0f, 1f));
                camera.localRotation = m_CameraTargetRot;
            }
            // Rotate according to axes
            else
            {
                // If there's a joystick, use that. Otherwise use mouse axes

                string[] jNames = Input.GetJoystickNames();

                if (jNames.Length > 0 && jNames[0] != "")
                {
                    xAxis = "Joystick Look X";
                    yAxis = "Joystick Look Y";
                }
                else
                {
                    xAxis = "Mouse X";
                    yAxis = "Mouse Y";
                }

                float yRot = CrossPlatformInputManager.GetAxis(xAxis) * XSensitivity;
                float xRot = CrossPlatformInputManager.GetAxis(yAxis) * YSensitivity;

                m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
                m_CameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

                if (clampVerticalRotation)
                {
                    m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);
                }

                if (smooth)
                {
                    character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
                        smoothTime * Time.deltaTime);
                    camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot,
                        smoothTime * Time.deltaTime);
                }
                else
                {
                    character.localRotation = m_CharacterTargetRot;
                    camera.localRotation = m_CameraTargetRot;
                }

                //UpdateCursorLock();
            }
        }

        public void SetCursorLock(bool value)
        {
            lockCursor = value;
            if (!lockCursor)
            {
                // We force unlock the cursor if the user disables the cursor locking helper
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        public void UpdateCursorLock()
        {
            // If the user set "lockCursor" we check & properly lock the cursor
            if (lockCursor)
            {
                InternalLockUpdate();
            }
        }

        private void InternalLockUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                m_cursorIsLocked = false;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                m_cursorIsLocked = true;
            }

            if (m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (!m_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1f;

            float angleX = 2f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        private void OnGameOver()
        {
            SetCursorLock(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Messenger.RemoveListener(GameEvent.GameOver, OnGameOver);
        }
    }
}
