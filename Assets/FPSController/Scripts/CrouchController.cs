﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Crouch controller class. Checks if changing standing/crouching position is possible.
/// </summary>
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class CrouchController : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float crouchingHeight;
        [SerializeField]
        private float standingHeight;
        [SerializeField]
        private float camStandingHeight;
        [SerializeField]
        private float camCrouchingHeight;

        private Camera mainCamera;
        private CharacterController characterController;
        private bool isCrouchTransitionInProgress = false;
        private bool isCrouching = false;

        private void Start()
        {
            characterController = GetComponent<CharacterController>();
            mainCamera = Camera.main;
        }

        /// <summary>
        /// Check for crouch button input, change stance if possible. If transition is in progress,
        /// calculate lerp target position and lerp (except when character in air).
        /// </summary>
        private void Update()
        {
            if (CrossPlatformInputManager.GetButtonDown("Crouch") && CanChangePosition())
            {
                if (isCrouching)
                {
                    isCrouching = false;
                    characterController.height = standingHeight;
                    characterController.center = new Vector3(0f, 1f, 0f);
                }
                else
                {
                    isCrouching = true;
                    characterController.height = crouchingHeight;
                    characterController.center = new Vector3(0f, 0.5f, 0f);
                }

                isCrouchTransitionInProgress = true;
            }

            // If crouch transition in progress, calculate stand and crouch cam positions
            if (isCrouchTransitionInProgress)
            {
                Vector3 camPosition = mainCamera.transform.position;
                Vector3 standCamPosition = new Vector3(camPosition.x, characterController.
                    transform.position.y + camStandingHeight, camPosition.z);
                Vector3 crouchCamPosition = new Vector3(camPosition.x, characterController.
                    transform.position.y + camCrouchingHeight, camPosition.z);

                // If character is grounded, do crouch lerp
                if (characterController.isGrounded)
                {
                    if (isCrouching)
                    {
                        CamLerpToPosition(camPosition, crouchCamPosition);
                    }
                    else
                    {
                        CamLerpToPosition(camPosition, standCamPosition);
                    }
                }
            }
        }

        /// <summary>
        /// Lerp the camera to crouching or standing position.
        /// </summary>
        /// <param name="currentPosition">Current cam position</param>
        /// <param name="targetPosition">Target cam position</param>
        private void CamLerpToPosition(Vector3 currentPosition, Vector3 targetPosition)
        {
            mainCamera.transform.position = Vector3.Lerp(currentPosition, targetPosition, Time.fixedDeltaTime
                * speed);

            if (Mathf.Abs(transform.position.y - targetPosition.y) < 0.01f)
            {
                isCrouchTransitionInProgress = false;
                //Debug.Log("Reached " + (isCrouching ? "crouching" : "standing") + " height");
            }
        }

        /// <summary>
        /// Check if character is allowed to crouch or stand up.
        /// </summary>
        /// <returns>True if character is allowed to change position</returns>
        private bool CanChangePosition()
        {
            // If not grounded, can't change position
            if (!characterController.isGrounded)
            {
                return false;
            }

            // If trying to stand up, but space above character is obstructed, can't stand up
            if (isCrouching)
            {
                return Physics.SphereCastNonAlloc(new Vector3(characterController.transform.position.x,
                     characterController.transform.position.y + characterController.radius,
                     characterController.transform.position.z), characterController.radius,
                     Vector3.up, new RaycastHit[2], standingHeight - 2f * characterController.radius) == 1;
            }
            else
            {
                return true;
            }
        }
    }
}