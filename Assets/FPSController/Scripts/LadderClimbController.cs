﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Ladder climb controller class. Checks ladder climb conditions.
/// </summary>
public class LadderClimbController : MonoBehaviour
{
    [SerializeField]
    private float climbingSpeed;

    private bool isClimbingLadder = false;
    private bool isCollidingWithLadder = false;
    private CharacterController characterController;
    private Transform ladderTransform;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    /// <summary>
    /// Check if current conditions effect ladder climbing. If climbing ladder, use vertical
    /// translation for player.
    /// </summary>
    private void Update()
    {
        // If ladder collision ended or jumping while climbing is on, turn of ladder climbing
        if (isClimbingLadder)
        {
            if (!isCollidingWithLadder || CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                ToggleLadderClimbing(false);
            }
        }
        // If colliding with ladder in the right angle and moving vertically or in air, turn on ladder
        // climbing
        else if (isCollidingWithLadder)
        {
            if (Vector3.Dot(ladderTransform.forward, transform.forward) >= 0.9f &&
                (CrossPlatformInputManager.GetAxis("Vertical") > 0f || !characterController.isGrounded))
            {
                ToggleLadderClimbing(true);
            }
        }

        if (isClimbingLadder)
        {
            transform.Translate(Vector3.up * CrossPlatformInputManager.GetAxis("Vertical") *
                climbingSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Toggle ladder climbing flag and broadcast related event.
    /// </summary>
    /// <param name="isEnabled"></param>
    private void ToggleLadderClimbing(bool isEnabled)
    {
        isClimbingLadder = isEnabled;
        Messenger<bool>.Broadcast(GameEvent.LadderToggleMovement, !isEnabled);
    }

    /// <summary>
    /// If colliding with ladder set the flag to true.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Tags>().Ladder)
        {
            isCollidingWithLadder = true;
            ladderTransform = other.transform;
        }
    }

    /// <summary>
    /// If not colliding with ladder anymore set the flag to false.
    /// </summary>
    /// <param name="other">Other collider</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Tags>().Ladder)
        {
            isCollidingWithLadder = false;
        }
    }
}
