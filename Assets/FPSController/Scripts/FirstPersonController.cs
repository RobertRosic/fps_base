using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    /// <summary>
    /// Main FPS controller class.
    /// </summary>
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;
        private bool m_ShouldResetCamRotation = false;
        private bool m_IsMovementEnabled = true;
        private bool m_IsCameraRotationEnabled = true;
        private bool m_IsRunToggled = false;
        private Vector3 m_Impact = Vector3.zero;
        private float m_mass = 3f;

        // Use this for initialization
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle / 2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
            m_MouseLook.Init(transform, m_Camera.transform);

            m_MouseLook.SetCursorLock(true);
            m_MouseLook.UpdateCursorLock();
        }

        // Update is called once per frame
        private void Update()
        {
            if (CrossPlatformInputManager.GetButton("Run Toggle"))
            {
                m_IsRunToggled = !m_IsRunToggled;
            }

            // Rotate camera or reset rotation if needed
            if (m_IsCameraRotationEnabled)
            {
                RotateView(m_ShouldResetCamRotation);
                m_ShouldResetCamRotation = false;
            }

            // The jump state needs to be read here to make sure it is not missed
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

            // Player has landed on the ground
            if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
            {
                StartCoroutine(m_JumpBob.DoBobCycle());

                if(m_Impact.magnitude < 0.2f)
                {
                    PlayLandingSound();
                }
                
                m_MoveDir.y = 0f;
                m_Jumping = false;
            }

            if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;

            // Apply knockback
            if (m_Impact.magnitude > 0.2f)
            {
                m_CharacterController.Move(m_Impact * Time.deltaTime);
            }

            // Reduce the impact in each update
            m_Impact = Vector3.Lerp(m_Impact, Vector3.zero, 5f * Time.deltaTime);
        }

        private void PlayLandingSound()
        {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }

        /// <summary>
        /// Main movement logic is invoked here.
        /// </summary>
        private void FixedUpdate()
        {
            if (m_IsMovementEnabled)
            {
                float speed;
                GetInput(out speed);
                // always move along the camera forward as it is the direction that it being aimed at

                Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

                // get a normal for the surface that is being touched to move along it
                RaycastHit hitInfo;
                Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                                   m_CharacterController.height / 2f, ~0, QueryTriggerInteraction.Ignore);
                desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

                m_MoveDir.x = desiredMove.x * speed;
                m_MoveDir.z = desiredMove.z * speed;

                // Manage gravity pull
                if (m_CharacterController.isGrounded)
                {
                    m_MoveDir.y = -m_StickToGroundForce;

                    if (m_Jump)
                    {
                        m_MoveDir.y = m_JumpSpeed;
                        PlayJumpSound();
                        m_Jump = false;
                        m_Jumping = true;
                    }
                }
                else
                {
                    m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
                }

                if (m_CharacterController.enabled == true)
                {
                    m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);
                }

                ProgressStepCycle(speed);
                UpdateCameraPosition(speed);

                // TODO this is now disabled (not needed at the moment)
                //m_MouseLook.UpdateCursorLock();
            }
        }

        /// <summary>
        /// Invoke play footstep sound method of a step interval has been reached.
        /// </summary>
        /// <param name="speed">Current player speed</param>
        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0f && (m_Input.x != 0f || m_Input.y != 0f))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ?
                    1f : m_RunstepLenghten))) *
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }

        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }

        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }

        /// <summary>
        /// Move camera according to head bob.
        /// </summary>
        /// <param name="speed">Current player speed</param>
        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;

            if (!m_UseHeadBob)
            {
                return;
            }

            if (m_CharacterController.velocity.magnitude > 0f && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                      (speed * (m_IsWalking ? 1f : m_RunstepLenghten)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
            m_Camera.transform.localPosition = newCameraPosition;
        }

        /// <summary>
        /// Get input axis data, manage running and walking state, FOV kick if enabled.
        /// </summary>
        /// <param name="speed">Current speed to be used (walking or running)</param>
        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            bool waswalking = m_IsWalking;

            if (m_IsRunToggled)
            {
                m_IsWalking = false;
            }
            else
            {
#if !MOBILE_INPUT
                // On standalone builds, walk/run speed is modified by a key press.
                // keep track of whether or not the character is walking or running
                m_IsWalking = !Input.GetButton("Run");
#else
                m_IsWalking = true;
#endif
            }

            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }

        private void RotateView(bool shouldResetCamRotation)
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform, shouldResetCamRotation);
        }

        /// <summary>
        /// Push the player away if mob is hit.
        /// </summary>
        /// <param name="hit">The other collider hit</param>
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;

            if (hit.gameObject.layer == LayerMask.NameToLayer("Mob"))
            {
                AddKnockback(hit.normal, 30f);
            }
        }

        /// <summary>
        /// Add a knockback force to the character controller
        /// </summary>
        /// <param name="dir">Direction</param>
        /// <param name="force">Force</param>
        private void AddKnockback(Vector3 dir, float force)
        {
            dir.Normalize();

            if (dir.y < 0)
            {
                dir.y = -dir.y; // Reflect down force on the ground
            }

            m_Impact += dir.normalized * force / m_mass;
        }

        private void OnDestroy()
        {
            m_MouseLook.Dispose();
        }        

        private void OnEnable()
        {
            Messenger.AddListener(GameEvent.ClimbUpStarted, OnClimbUpStarted);
            Messenger.AddListener(GameEvent.ClimbUpFinished, OnClimbUpFinished);
            Messenger<bool>.AddListener(GameEvent.LadderToggleMovement, OnLadderToggleMovement);
        }

        private void OnDisable()
        {
            Messenger.RemoveListener(GameEvent.ClimbUpStarted, OnClimbUpStarted);
            Messenger.RemoveListener(GameEvent.ClimbUpFinished, OnClimbUpFinished);
            Messenger<bool>.RemoveListener(GameEvent.LadderToggleMovement, OnLadderToggleMovement);
        }

        private void OnClimbUpStarted()
        {
            m_ShouldResetCamRotation = true;
            m_IsMovementEnabled = false;
            StartCoroutine(WaitBeforeRotationDisabled());
        }

        private void OnClimbUpFinished()
        {
            m_IsMovementEnabled = true;
            m_IsCameraRotationEnabled = true;
        }

        private void OnLadderToggleMovement(bool isMovementEnabled)
        {
            m_IsMovementEnabled = isMovementEnabled;
        }

        private IEnumerator WaitBeforeRotationDisabled()
        {
            yield return new WaitForEndOfFrame();
            m_IsCameraRotationEnabled = false;
        }
    }
}
